def rps(p1, p2)
  rules = {'rock' => 'scissors', 'scissors' => 'paper', 'paper' => 'rock'}
  case
    when rules[p1] == p2 then 'Player 1 won!'
    when rules[p2] == p1 then 'Player 2 won!'
    else 'Draw!'
  end
end